﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        //variables
        List<BasketballPlayer> listOfPlayers = new List<BasketballPlayer>();

        public Form1()
        {
            InitializeComponent();
            
        }
        // when enter is clicked - store the information as a new Basketball player object.
        private void button1_Click(object sender, EventArgs e)
        {
            //convert string values for height and position to integers.
            int posTemp = int.Parse(textBox2.Text);
            int heightTemp = int.Parse(textBox3.Text);

            //create the object with the information from the text boxes
            listOfPlayers.Add(new BasketballPlayer(textBox1.Text, posTemp, heightTemp, textBox4.Text));

            //feedback of who has been added.
            label5.Text = textBox1.Text + " has been added to the system";

        }

        //display the players in the text box by running through the list of objects.
        private void button2_Click(object sender, EventArgs e)
        {
            // loop through the list of objects. Display the object properties in the text box.
            for (int i = 0; i < listOfPlayers.Count; i++)
            {
                textBox5.AppendText("Name: " + listOfPlayers.ElementAt(i).getName() + "\r\n");
                string temp = listOfPlayers.ElementAt(i).getPosition().ToString();
                textBox5.AppendText("Position: " + temp + "\r\n");
                temp = listOfPlayers.ElementAt(i).getHeight().ToString();
                textBox5.AppendText("Height: " + temp + "\r\n");
                textBox5.AppendText("Grade: " + listOfPlayers.ElementAt(i).getGrade() + "\r\n" + "\r\n");

            }
        }
    }
}
