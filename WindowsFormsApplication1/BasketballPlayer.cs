﻿namespace WindowsFormsApplication1
{
    internal class BasketballPlayer
    {
        // variables for the class
        private string name;
        private int position;
        private int height;
        private string grade;
    

        //constructor that builds the object - stores the info coming in in the variables.
        public BasketballPlayer(string nm, int pos, int hght, string grd)
        {
            name = nm;
            position = pos;
            height = hght;
            grade = grd;

        }

        // ------------------- accessors and modifiers for the variables -----------------
        //return name value
        public string getName ()
        {
            return name;
        }
        //modify name value
        public void setName(string val)
        {
            name = val;
        }

        //return name value
        public int getPosition()
        {
            return position;
        }
        //modify name value
        public void setPosition(int val)
        {
            position = val;
        }

        //return name value
        public int getHeight()
        {
            return height;
        }
        //modify name value
        public void setHeight(int val)
        {
            height = val;
        }

        //return name value
        public string getGrade()
        {
            return grade;
        }
        //modify name value
        public void setGrade(string val)
        {
            grade = val;
        }

    }
}